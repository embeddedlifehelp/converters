#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "stdint.h"

void converthex(char *data, unsigned int a); //declaration

int main(int argc, char *argv[]){
      unsigned int x=0;
      uint8_t i=0;
      char temp[10];
      char text[100];

      //example 1
      printf("print simple number to hex ascii\r\n");
      x = 255;
      converthex(temp, x);
      printf("%d is converted into %s\r\n",x,temp);
        printf("\r\n");


      //example 2
      printf("print character to hex ascii\r\n");
      x = 'c';
      converthex(temp, x);
      printf("%c is converted into %s\n",x,temp);
        printf("\r\n");


      //example 3
      printf("print string to comma separated hex ascii\r\n");
      strcpy(text,"hello world!");
      i=0;
      printf("%s is converted into\r\n",text);
      while(text[i]!=0){
        converthex(temp, text[i]);
        printf("%s,",temp);
        i++;
      }
      printf("\r\n\a");
      system("PAUSE");
      return 0;
}


void converthex(char *data, unsigned int a){ //definition
   unsigned int temp =a;
   int digits=0,set;
   int i=0;
   unsigned int x;
   unsigned int mask;
   mask=0x0f;
   while(temp!=0){
        temp=temp/16;
        digits++;
        if(digits>1)mask=mask<<4;
   }
   set=digits;
   set--;

   data[i]='0';
   i++;
   data[i]='x';
   i++;
   temp=a;
   while(digits!=0){
        x=(temp&mask);
        x=x>>(set*4);
        if(x<=9)data[i]=0x30+x;
        else{
            data[i]=0x41+(x-10);
        }
        digits--;
        i++;
        temp=temp<<4;
   }
    data[i]='\0';
}
